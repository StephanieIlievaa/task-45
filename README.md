# ⚙ HTML, CSS and SASS Boilerplate 
This project is used as a boilerplate for tasks in the "HTML, CSS and SASS" course in boom.dev

🤯💥💣
## HTML & CSS Task
**Using Google Fonts(local)**

##Objective
* Checkout the dev branch.
* Add the "Roboto" font from google fonts locally
* Set the HTML document's font to Roboto, sans-serif
* When implemented merge the dev branch to master.


##Requirements
* Start the project with **npm run start**.
* The "src/fonts" folder must contain the font file for Roboto
* The font must be defined in the "src/scss/base/_fonts.scss"

##Gotchas
* https://google-webfonts-helper.herokuapp.com/fonts
